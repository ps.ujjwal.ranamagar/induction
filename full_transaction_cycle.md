### Full Transaction Cyclefull-cycle-screenshots

Follow the following steps to complete the full transaction cycle:

1. Run the EPG activemq and tomcat first. Then only start CORPAY activemq and tomcat.

2. After successful running of all the applications, login to CORPAY using `maker@mk` login details.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/full-cycle-screenshots/ss1-maker-login.png"><br/><br/>

3. Go to `Payment -> Payment Template`. Then payment dashboard will be displayed.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/full-cycle-screenshots/ss2-maker-payment.png"><br/><br/>

4. Click on `New item` icon and fill up the payment form as shown below. Choose `ONUS Payment`, any one of `Ordering Account`, the `Payment Total Amount` should not exceed your `Ordering Account` balance and `Submission Start Date` as of today. Leave other options as it is and press create.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/full-cycle-screenshots/ss3-maker-new-payment.png"><br/><br/>

6. After you press create button payment details will be displayed. Now add new transaction by clicking on `New item` button under `Transactions` heading. Then a transaction detail form will be displayed.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/full-cycle-screenshots/ss4-select-transaction.png"><br/><br/>

8. Fill up the form. Put the same digits on `Amount` as you did on ``Payment Total Amount` in the payment form. Click on the search icon in `Beneficiary Account` and you will be displayed the accounts. Select the `Chhanda` account for this exercise. Then click create button.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/full-cycle-screenshots/ss5-beneficiary-account.png"><br/><br/>

9. Click on the payment number that will direct you to the payment details page.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/full-cycle-screenshots/ss7-go-to-payment.png"><br/><br/>

10. Click on proceed and check the payment on the dashboard. Search the transaction and see if it is there or not.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/full-cycle-screenshots/ss8-maker-transaction.png"><br/><br/>

11. Now go login to CORPAY with `checker@mk` details.

12. Go to `Payment -> Payment Template` and search the newly made transaction.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/full-cycle-screenshots/ss9-checker-check-trasaction.png"><br/><br/>

13. Double click on that payment and click on `Proceed`.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/full-cycle-screenshots/ss10-checker-proceed.png"><br/><br/>

14. The transaction will now be reflected in EPG. So login to EPG with `full@hbl` user details.

15. After logging in EPG, go to `Outward -> Credit -> Manual` and search for the recently made transaction. You will see that it has been posted.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/full-cycle-screenshots/ss11-epg-check.png"><br/><br/>