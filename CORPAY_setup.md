#### Configuration

Create two seperate folders as "corpay-activemq" and "corpay-tomcat" in `C:` drive. Then extract activeMQ and tomcat zip to their respective folders. After all the required applications and tools are installed, then only proceed to make the followings configurations:

1. Copy and paste the provided ROPS backup file to `C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\Backup`. Then restore the database in `MS SQL Server Management Studio` (SSMS).

<img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/corpay-screenshots/ss1-restore-database.png"><br/><br/>

2. Create a new login in SQL Server named "ROPS" with a password of "ROPS".

    ```
    CREATE LOGIN ROPS
    WITH PASSWORD = 'ROPS',
    CHECK_POLICY = OFF,
    CHECK_EXPIRATION = OFF,
    DEFAULT_DATABASE = ROPS;
    ```

3. Add the SQL Server login "ROPS" to the "sysadmin" server role

    ```
    EXEC sp_addsrvrolemember
    @loginame = N'ROPS',
    @rolename = N'sysadmin';
    ```

4. Create a new user in the "ROPS" database that is mapped to the SQL Server login "ROPS", and adds the user to the "db_owner" role, giving the user full permissions to manage and modify the "ROPS" database.

    ```
    use ROPS
    CREATE USER ROPS FROM LOGIN ROPS;
    EXEC sp_addrolemember
    N'db_owner', 'ROPS';
    ```

5. Change the access mode of the "ROPS" database from single-user mode to multi-user mode.

    ```
    ALTER DATABASE ROPS SET MULTI_USER;
    ```

<img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/corpay-screenshots/ss2-create-user.png"><br/><br/>


6. Set the environment variables using command prompt.

    ```
    setx /m db_url1 "jdbc:jtds:sqlserver://localhost:1433/ROPS"
    setx /m db_user1 "ROPS"
    setx /m db_password1 "ROPS"
    setx /m db_schema1 "ROPS"
    ```

7. Verify if the environment variables using "echo" command.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/corpay-screenshots/ss3-set-env-variables.png"><br/><br/>

8. Extract the provided WAR file. Copy the "ROPS" folder and paste it inside the webapps folder of tomcat for CORPAY.

   <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/corpay-screenshots/ss4-copy-rops-folder.png"><br/><br/>


9. Remove the comment on openwire and make it active while others are commented from line 114 to 117 in **activemq.xml** file located in `C:\epg\corpay-activemq\apache-activemq-5.13.0\conf`. Also make change port no **61616** to **61617**.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/corpay-screenshots/ss5-edit-activemq-xml.png"><br/><br/>


10. Change the `jettyPort` value to **8162** in **jetty.xml** file which is also located in `C:\corpay\corpay-activemq\apache-activemq-5.13.0\conf`. 

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/corpay-screenshots/ss6-edit-jetty-xml.png"><br/><br/>

11. Edit the connector port number to **9999** and shutdown port to **9009** in the **server.xml** file of tomcat for CORPAY which is located in `C:\corpay\corpay-tomcat\apache-tomcat-8.5.87\conf`.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/corpay-screenshots/ss7-edit-server-xml.png"><br/>

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/corpay-screenshots/ss8-edit-server-xml2.png"><br/><br/>

12. Check if all the environment variables are well set in **applicationContext-datasources.xml** which is located in `C:\corpay\corpay-tomcat\apache-tomcat-8.5.87\webapps\ROPS\WEB-INF\classes\config\core\spring`.

```
<bean id="jfwDataSource" class="org.apache.commons.dbcp.BasicDataSource">  
    <property name="driverClassName" value="${db_driver}" />  
    <property name="url" value="${db_url1}" />  
    <property name="username" value="${db_user1}" />  
    <property name="password" value="${db_password1}" />  
    <property name="validationQuery" value="${db_validation_query}" />  
    <property name="maxWait" value="10000" /> <property name="maxIdle" value="30" />  
    <property name="testOnBorrow" value="true"></property>  
    <property name="testOnReturn" value="true"></property>  
    <property name="maxActive" value="100" /> 
</bean>
```
 <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/corpay-screenshots/ss9-edit-applicationContext-datasources-xml.png"><br/><br/>

13. Open **datasources.properties** file in `C:\corpay\corpay-tomcat\apache-tomcat-8.5.87\webapps\ROPS\WEB-INF\properties` and verify the environment variables used to connect the DB.

    ```
    #oracle : jdbc:oracle:thin:@ip:1521:PSDB
    #MsSql : jdbc:jtds:sqlserver://ip:1433/dbname
    #LOG4JDBC : add log4jdbc: after jdbc:
    connection.url=db_url1

    datasource.user=db_user1
    datasource.password=db_password1
    datasource.schema=db_schema1
    ```
 <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/corpay-screenshots/ss10-edit-datasources-properties.png"><br/><br/>


### Running CORPAY

1. To run CORPAY first run EPG-activeMQ and EPG-tomcat. Then only start activeMQ for CORPAY which will run in port **8162**.

    `http://localhost:8162/admin/`

    use default username and password which is "admin" to log in.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/corpay-screenshots/ss11-corpay-activemq.png"><br/><br/>

2. After successful running of CORPAY-activeMQ, start tomcat for CORPAY which will run in port **9999**.

   `http://localhost:9999/ROPS/`

   <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/corpay-screenshots/ss12-corpay-tomcat.png"><br/><br/>

### Logging In CORPAY

1. Before logging in CORPAY, run the following queries to update passwords for usernames **ROPSMAKERADMIN@ROPS** and **ROPSCHECKERADMIN@ROPS** :

    ```
    update jfw_users set password='17cc5195319e5a7e2b1347e49bc9ce5c' where username = 'ROPSMAKERADMIN@ROPS';
    ```

    ```
    update jfw_users set password='0fc11d64a8553b283948c3224f1ef010' where username = 'ROPSCHECKERADMIN@ROPS';
    ```
    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/corpay-screenshots/ss13-update-user-details.png"><br/><br/>

2. Then login using the updated details;

    username: **ROPSCHECKERADMIN@ROPS**  
    password: 123  
    tenant: ROPS  
    Locale: English  

    The application will prompt to change the password if the details are correct. If so change the password and use the new password to log in.
 
    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/corpay-screenshots/ss14-logging-in.png"><br/>

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/corpay-screenshots/ss15-successful-login.png"><br/>

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/corpay-screenshots/ss16-corpay-dashboard.png"><br/><br/>


 
