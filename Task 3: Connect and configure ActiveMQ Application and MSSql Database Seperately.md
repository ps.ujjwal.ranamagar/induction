### Task 3: Connect and configure ActiveMQ Application and MSSql Database Seperately

Follow the steps below to use MSSQL as the ActiveMQ db:

1. First download the JDBC drive for MSSQL and copy the jar file to the ActiveMQ/lib/optional directory.

<img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/tasks-screenshots/task3-1-jdbc-drive.png"><br/><br/>

2. Edit activemq.xml configuration as follows to setup activemq db.

``` xml
<bean id="mssql-ds" class="org.apache.commons.dbcp2.BasicDataSource" destroy-method="close">
    <property name="driverClassName" value="com.microsoft.sqlserver.jdbc.SQLServerDriver" />
    <property name="url" value="jdbc:sqlserver://localhost:1433;DatabaseName=epgmq" />
    <property name="username" value="sa" />
    <property name="password" value="ujjwal@123" />
    <property name="poolPreparedStatements" value="true" />
</bean>
```
``` xml
<persistenceAdapter>
    <jdbcPersistenceAdapter dataDirectory="${activemq.data}" dataSource="#mssql-ds" lockKeepAlivePeriod="0">
        <adapter>
            <transact-jdbc-adapter />
        </adapter>
        <locker>
            <lease-database-locker lockAcquireSleepInterval="10000" dataSource="#mssql-ds">
                <statements>
                    <statements lockTableName="activemq_lock" />
                </statements>
            </lease-database-locker>
        </locker>
    </jdbcPersistenceAdapter>
</persistenceAdapter>

```
<img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/tasks-screenshots/task3-2-activemqconf.png"><br/><br/>
<img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/tasks-screenshots/task3-3-activemqconf.png"><br/><br/>

3. Reenable TLS 1 and 1.1 support for JAVA. go to `C:\Program Files\OpenJDK\jdk-8.0.292.10-hotspot\jre\lib\security` and edit `java.security` file.

<img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/tasks-screenshots/task3-6-java-security.png"><br/><br/>

4. Open SSMS and create a new `epgmq` database. The database name should be same as it was set up in activemq.xml. Open the database to see if it working.

<img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/tasks-screenshots/task3-4-new-db.png"><br/><br/>
<img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/tasks-screenshots/task3-5-epgmqtable.png">



