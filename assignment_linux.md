## 1. Write a Linux command to list all files in a directory in details (Including hidden files)

To list all the files in a directory in details including hidden files we can use command `ls` with flags `-l` and `-a`. The `-a` option displays hidden files, and the `-l` option lists files in a long format that includes detailed information about each file.

    $ ls -la

## 2. Explain how to tell if a file is hidden or not in Linux.

In Linux, files and directories can be marked as hidden by starting their names with a dot `(.)` character. 

## 3. Write a command that creates a file named my-name.txt and contains your first and last name (use only one command).

The `echo` command is used to print the text string to the terminal, which is then redirected to a file named `my-name.txt` using the `>` operator.

    $ echo "Ujjwal Rana" > my-name.txt

## 4. Write a command to delete a file name test.txt and another command to delete a folder name “test-folder”.

To delete the file named `test.txt` and folder named `test-folder` we can use `rm` (remove) command with `-r` (recursive) flag. `rm -r` allows to remove directories and their contents.

    $ rm test.txt && rm -r test-folder

## 5. Write a command to copy all .txt files from “/home/usr/test” to “home/usr/test1” (one command).

To copy all `.txt` files from `/home/usr/test` directory to `/home/usr/test`, we use the `cp` (copy) command and to match a specific pattern, we use a wildcard character `*`.

    $ cp /home/usr/test/*.txt /home/usr/test1/

## 6. Explain how to get your IP address in Linux.

`ip addr show` command will display detailed information about all network interfaces on your system, including their IP addresses.

    $ ip addr show

## 7. Write a command to check the disk usage as human output.

To  check the disk usage as human output, we can use the `du` (disk usage) command with the `-h` (human-readable) option.

    $ du -h

## 8. How could one search for the file progressoft.txt under the directory /home.

To find `progressoft.txt` in /home directory, we can use `find` command with flags `-L` and `-iname` which makes the command follow symbolic links and should match a pattern in the file name respectively. The search is case insensitive.

    $ find -L /home -iname progressoft.txt

## 9. Write a command that changes the permissions for a specific folder and give it only read and write permissions.

To change the permissions for a specific folder and give it only read and write permissions, we can use command `chmod` and assign `666` permission followed by `-R` flag to assign the permission to all the files in `my-folder` recursively.

    $ chmod 666 -R my-folder

## 10. Write a command that makes a file named script.sh as executable.

The command `chmod` with `+x` permission will make the file `script.sh` executable.
    
    $ chmod +x script.sh

## 11. List two commands to provide help for a specific Linux command.

To display help for a specific command, we can use either the `man` command or the `info` command.

## 12. Write a command that used to make a shell variable known to subsequently executed programs.

We can use the `export` command. To add `/home/clusus/.local/bin` to the `$PATH` variable.
    
    $ export PATH=$PATH:/home/clusus/.local/bin

## 13. What is two-character sequence is present at the beginning of an interpreted script? Please specify two characters only.

The two characters that are present at the beginning of an interpreted script are `#!`.

## 14. Write a command that moves the directory ~/PS-Exam and its content to ~/PS-New/2021

We can use the `mv` command to move a directory and all its contents.
`
    $ mv ~/PS-Exam ~/PS-New/2021

## 15. Provide the file holds the definition of the local user accounts.

`/etc/passwd` stores the details of the local user accounts.

## 16. Write command can be used to extract files from an archive.

The tar utility can be used to extract an archive. The `-x` flag, which indicates that we are performing an extraction, must be used in conjunction with the `-f` flag to specify an input file in order to extract an archive. To specify whether the file has to be inflated as well, use the `-z` argument.

    $ tar -xzf sample.tar

## 17. Explain how to install telnet on Redhat and use it to check if a specific port is reachable or not (using terminal).

To install the telnet package in RHEL and associated systems, we can use the `dnf` package manager and pass the argument install along with the name of the package we need to install. To check if a specific port is reachable or not using `telnet` we can pass an `ip address` or `domain name` along with a `port number` as arguments to telnet.

    $ sudo dnf install telnet && telnet facebook.com 443

## 18. Provide three methods to find out the memory usage and two commands to find the cpu usage.

Memory usage can be displayed using `free -h`, `htop`, or `top`. `top` and `htop`can also be used to display CPU usage.

## 19. Create a user "Linux", that exists in a group called "practice".

To create a user in the Linux group, we use the `useradd` command. We can use the `-g` flag to assign the user Linux to the practice user group.
    
    $ sudo useradd -g practice Linux

## 20. Create a Directory called "welcome" in the Root Directory that has owner permissions to the user "Linux", and all recursive files are readable, writeable, and executable by the owner. Readable and executable by anyone in the group "practice".

To create a directory in the root directory with the owner set to Linux, we first create the directory as root user and then change the permissions and     ownership. To create a directory within the root directory we can use the `mkdir` command, `chmod` to give read, write and execute permissions to the user and read and execute permssions to other users and group and lastly we use `chown` to assign the owner and group. We will also use the -R flag with chown to change ownership recursively.

    $ sudo mkdir /welcome
    $ sudo chmod 755 /welcome
    $ sudo chown -R Linux:practice /welcome

## 21. Write a bash script in the directory "welcome", to print the numbers from 1-100, every number on a line. The script will amend the output to a file called numbers.txt

First we switch to user `Linux` using `sudo -u Linux` and open `vim` to create `script.sh` inside `/welcome`. Then we write the content for `script.sh` and run the script. 

    $ sudo -u Linux vim /welcome/script.sh
    $ sudo -u Linux chmod +x /welcome/script.sh

    ```
    #!/bin/bash
    for i in {1..100};
    do
    echo $i
    echo $i >> numbers.txt
    done

    ```
    $ /welcome/script.sh

## 22. Create a soft link to the generated file in the user home directory and can be read using the cat command

We can use the `ln` command with the `-s` flag to generate a soft link to the generated numbers.txt file. The `-s` flag denotes that the link will be a soft link.

    $ ln -s numbers.txt linked_numbers.txt

## 23. Solution:

- Three identical Virtual Machines were created in virtualbox with following specs:

`OS: RedHat Hard disk: 20GB Partitions: /boot 200 MB SWAP 2GB / Rest of space`

[Partition](https://gitlab.com/ps.ujjwal.ranamagar/induction/-/blob/main/screenshots/ss1-partition.png)  

- The hostname was changed using the following [HOSTNAME] command:

`$ echo "[HOSTNAME]" > sudo tee /etc/hostname`

The `tee` command directs the standard input to the standard output as well as writes the standard input to a file.

[Change-Hostname](https://gitlab.com/ps.ujjwal.ranamagar/induction/-/blob/main/screenshots/ss2-change-hostname.png)  

- To enable SSH, we should install `openssh-server` package using `$ sudo dnf install openssh-server` command. Then, we need to enable SSH service using `sudo systemctl enable sshd.service`. Finally, we should add a new firewall rule to allow ssh through the firewall using `$ sudo firewall-cmd --zone=public --permanent --add-service=ssh` command.

[Enable-ssh](https://gitlab.com/ps.ujjwal.ranamagar/induction/-/blob/main/screenshots/ss3-ssh-enable.png) 

- To log into the server using SSH, we need to use the ssh [USERNAME]@[ADDRESS] command. If we have generated an SSH key, we can use the key to log into the server without password by copying the SSH public key. This can be done using ssh-copy-id [USERNAME]@[HOSTNAME].

[Servers](https://gitlab.com/ps.ujjwal.ranamagar/induction/-/blob/main/screenshots/ss4-servers.png)  

- We must install the `vsftpd` package in order to configure FTP on RedHat. We must make the required changes to the configuration file located in `/etc/vsftpd/vsftpd.conf` after installing the `vsftpd` package. After that, we may start the systemd unit file `vsftpd.service`. After that, we must restart the `firewalld.service` component and add a firewall rule to permit ftp traffic into the server. To enable the FTP user to access the server, we must then create a new SELinux policy. For the sake of this demonstration, we'll turn on the `ftpd_full_access` boolean to grant the ftp user access to the entire root.

[Enable-ftp](https://gitlab.com/ps.ujjwal.ranamagar/induction/-/blob/main/screenshots/ss5-enable-ftp.png)  

- Logging into the server using ftp browsers like filezilla.

[Connect-filezilla](https://gitlab.com/ps.ujjwal.ranamagar/induction/-/blob/main/screenshots/ss6-filezilla.png)  

- Created a Linux user named "oracle" having its primary group as "oinstall" and Secondary Group as "dba". To create the oinstall and dba groups, the `sudo groupadd [GROUPNAME]` command is used. The user must be added and the proper main and secondary groups must be set using the `useradd -g oinstall -G dba oracle` command after the group has been created.

[User-oracle](https://gitlab.com/ps.ujjwal.ranamagar/induction/-/blob/main/screenshots/ss7-user-oracle.png)  

A grub password can be set using the `grub2-setpassword` command. Then we will have to regenerate the grub configuration file using `grub2-mkconfig -o /boot/grub2/grub.cfg`.

[Grub](https://gitlab.com/ps.ujjwal.ranamagar/induction/-/blob/main/screenshots/ss8-grub.png)