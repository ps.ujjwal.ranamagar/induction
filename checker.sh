#!/bin/bash

read -p "Enter an input: " input

# Check if the input is a number
if [ "$input" -eq "$input" ] 2>/dev/null; then
  echo "The input is a number."
# Check if the input is a string
elif [[ "$input" =~ [^[:digit:]] ]]; then
  echo "The input is a string."
# Check if the input is empty
elif [ -z "$input" ]; then
  echo "The input is empty."
# None of the above cases matched
else
  echo "The input is not a number, string or empty."
fi

#The read command is used to read user input and store it in the input variable.
#The if statement checks if the value of $input is equal to itself (-eq), which means that it is a number. The 2>/dev/null redirection is used to suppress any error messages.
#The script checks if the input is a string using a regular expression that matches any character that is not a digit ([^[:digit:]]).
#The script checks if the input is empty using the -z test.
#If none of the above cases match, the script outputs a message indicating that the input is not a number, string or empty.