### Task 2: Automatic deletion of temporary files inside tomcat and HBL_SITE offUsFolder

Follow the following steps to create a task scheduler to delete temporary files inside tomcat and HBL_SITE\offUsFolder:

1. Open `Task Scheduler` by searching in your windows machine.

2. Then click on `Create Task` which is on the right of your window.

3. Set the name and description of your task.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/tasks-screenshots/task2-1-create-task.png"><br/><br/>

4. Set the trigger as per your requirement. For now, it has been set for daily at 1:00 PM.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/tasks-screenshots/task2-2-create-trigger.png"><br/><br/>

5. Write a script to delete temporary files inside tomcat and HBL_SITE\offUsFolder and save it as a batch file. 

```
@echo off
del C:\HBL_SITE\offUsFolder\* /q /f
del C:\EPG\EPG Tomcat\temp\* /q /f
del C:\CORPAY\CORPAY Tomcat\temp\* /q /f

```
6. Upload the batch file as action.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/tasks-screenshots/task2-3-upload-script.png"><br/><br/>

7. Disable the only AC power option.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/tasks-screenshots/task2-4-power-condition.png"><br/><br/>

8. Run the task.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/tasks-screenshots/task2-5-run-task.png"><br/><br/>
