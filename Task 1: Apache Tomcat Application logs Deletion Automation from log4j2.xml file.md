### Task 1: Apache Tomcat Application logs Deletion Automation from log4j2.xml file

Follow the steps below to automate deletion of tomcat logs from log4j2.xml:

1. Open the log4j2.xml file from the path `C:\EPG\EPG Tomcat\webapps\hbl\WEB-INF\classes`.

2. Since log4j supports a delete action for rolling file appenders make these changes SizeBasedTriggeringPolicy size to "300 KB" and a delet action in DefaultRolloverStrategy. 

```xml
<Routing name="FILE">
<Routes pattern="FILE">
    <Route>
        <RollingFile name="FILE"
                        fileName="${LOGGING_PATH}/FILE/FILE.${date:yyyy-MM-dd}.log"
                        filePattern="${LOGGING_PATH}/FILE/ARCHEIVE/FILE.%d{yyyy-MM-dd}-%i.log.zip"
                        ignoreExceptions="false">
            <DisableDataBaseTableFilter onMatch="DENY" />
            <PatternLayout Pattern="${PATTERN}">
            </PatternLayout>
            <Policies>
                <SizeBasedTriggeringPolicy size="300 KB" />
                <TimeBasedTriggeringPolicy />
            </Policies>
            <!-- <DefaultRolloverStrategy compressionLevel="9" />-->
            <DefaultRolloverStrategy >
            <Delete basePath="${LOGGING_PATH}/FILE/ARCHEIVE" maxDepth="1">
                <IfFileName glob="FILE*.zip" />
                <IfLastModified age="p1d" />
            </Delete>
            </DefaultRolloverStrategy>
        </RollingFile>
    </Route>
</Routes>
</Routing>
```

<img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/tasks-screenshots/task1-1-log4j2.png"><br/><br/>

3. Save the newly made changes and run activemq. See the files in `C:\EPG\EPG Tomcat\logs\JFWLogs\FILE\ARCHEIVE`.

<img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/tasks-screenshots/task1-2-log4j2.png"><br/><br/>
