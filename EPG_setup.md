To setup PS-EPG, following OS and tools were installed in a virtual setup: 

- **Virtual Box:** 8 GB RAM, 120 GB HDD, 2 Core CPU
- **OS:** Windows server 2008 R2 Standard Service Pack 1
- **Database:** MS SQL Server 2012 Enterprise Edition
- **Application Server:** Apache Tomcat 8.5.57
- **Messaging Server Activemq 5.13.0**
- **Java Version:** openjdk version 1.8.0-292
- **Database Name:** hbl

### Configuration

Extract activeMQ and tomcat zip folders to `C:` drive. Install all the required applications and tools. Then only proceed to make the followings configurations:

1. First enable the TCP/IP in `SQL Netwok Configuration` in `SQL Server Configuration Manager`. Set the port number to `1433` which is the also the default port number. Then check whether the SQL services are running or not in the `SQL Server Services`.

<img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss1-SQL%20Netwok%20Configuration.png">

<img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss2-SQL%20Server%20Services.png"> <br> <br>


2. Copy and paste the provided EPGLIVE backup file to `C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\BackupRestore`. Then restore the database in `MS SQL Server Management Studio` (SSMS). 

<img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss3-EPGLIVE%20Backup.png">

<img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss4-EPGLIVE%20Restoration.png">

<br> <br>


3. Select `EPGLIVE` database and create a new login using following query:

    CREATE LOGIN HBL
    WITH PASSWORD = 'HBL',
    CHECK_POLICY = OFF,
    CHECK_EXPIRATION = OFF,
    DEFAULT_DATABASE = EPGLIVE;

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss5-New%20User.png">

    <br> <br>


4. Provide administrative privileges and ownership to a user in the HBL database and set the database to multi-user mode using the following query:

    EXEC sp_addsrvrolemember @loginame = N'HBL', @rolename = N'sysadmin';
    use HBL
    CREATE USER HBL FROM LOGIN HBL
    EXEC sp_addrolemember
    N'db_owner', 'HBL';

    `ALTER DATABASE EPGLIVE SET MULTI_USER;`

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss6-Providing%20Privileges.png">


    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss7-Alter%20Table.png">

    <br> <br>


5. Set environment variables using following commands. `setx` is a command-line utility that sets the value of an environment variable in the user or system environment and `/m` option sets the environment variable in the system environment, which makes it available to all users.

    - Set **db_driver** to "net.sourceforge.jtds.jdbc.Driver". This is a Java Database Connectivity (JDBC) driver for SQL Server that enables a Java application to connect to a SQL Server database.

        `setx /m db_driver "net.sourceforge.jtds.jdbc.Driver"`

    - Set **db_url** to "jdbc:jtds:sqlserver://localhost:1433/HBL". This is the JDBC connection URL that specifies the database server and database name to connect to.

        `setx /m db_url "jdbc:jtds:sqlserver://localhost:1433/EPGLIVE"`

    - Set **db_user** to "HBL". This is the database username that will be used to authenticate the connection.

        `setx /m db_user "HBL"`

    - Set **db_password** to "HBL". This is the password for the database user.

        `setx /m db_password "HBL"`

    - Set **db_schema** to "db_owner". This is the default schema name to be used in the database.

        `setx /m db_schema "db_owner"`

    - Set **db_type** to "MSSQL2008". This specifies the type of database being used.

        `setx /m db_type "MSSQL2008"`

    - Set **db_validation_query** to "SELECT 1". This is a query that will be executed to validate the database connection.

        `setx /m db_validation_query "SELECT 1"`

    - Set **db_check_query** to "SELECT COUNT FROM INFORMATION_SCHEMA.TABLES". This is a query that will be executed to check if the database is accessible.

        `setx /m db_check_query "SELECT COUNT FROM INFORMATION_SCHEMA.TABLES"`

    - Set **db_dialect** to "org.hibernate.dialect.SQLServer2008Dialect". This specifies the Hibernate SQL dialect to use for SQL Server 2008.

        `setx /m db_dialect "org.hibernate.dialect.SQLServer2008Dialect"`

    - Set **java_opts** "-server -Xms1024m -Xmx6144m". These are Java runtime options that specify the minimum and maximum heap size for the Java Virtual Machine.

        `setx /m java_opts "-server -Xms1024m -Xmx6144m"`

6. Test whether the variable has been set or not using `echo` command.
    
    `echo %java_opts%`

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss8-Environment%20Variables.png">

    <br> <br>


7. Extract the provided WAR file. Copy the "hbl" folder and paste it inside the webapps folder of tomcat.

<img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss9-Hbl%20Folder.png">

<br> <br>


8. Open **server.xml** file from `conf` folder of `tomcat` and edit the connector port to **"8088"**.

<img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss10-Port%20Change%20For%20Tomcat.png">

<br> <br>


9. Check whether there is any ip address mentioned on these XML files: 

    **push-action-camel-config.xml** : `C:\epg\Tomcat 8.5\webapps\hbl\WEB-INF\classes\config\core\camel\push-action-camel-config.xml`

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss11-IP%20Check1.png">
    
    <br> <br>


    **applicationContext-integration.xml** : `C:\epg\Tomcat 8.5\webapps\hbl\WEB-INF\classes\config\core\spring\applicationContext-integration.xml`

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss12-IP%20Check2.png">

    <br> <br>


    **app-camel-config.xml** : `C:\epg\Tomcat 8.5\webapps\hbl\WEB-INF\classes\config\app\camel\app-camel-config.xml`

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss13-IP%20Check%203.png">

    <br> <br>


    Set all to "localhost" if found.

    Comment lines from 114 to 117 in **activemq.xml** file in `C:\epg\epg-activemq\apache-activemq-5.13.0\conf`

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss14-Commenting%20Lines.png">

    <br> <br>

### Running EPG

1. First run the **activemq** batch file to start activeMQ. It wll run at port `8161`. The file will be in the **bin** folder.
    
    `C:\epg\epg-activemq\apache-activemq-5.13.0\bin\win64`

    `http://localhost:8161/`

    Use default username and password `admin` to login.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss15-Running%20ActiveMQ.png">

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss16-ActiveMQ%20Login.png">

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss17-ActiveMQ%20LoggedIn.png">

    <br> <br>


2. After activeMQ is running successfully, start tomcat using its `startup` batch file which is inside the **bin** folder of tomcat. It will run in port `8088`.

    `C:\epg\epg-tomcat\apache-tomcat-8.5.87\bin`

    `http://localhost:8088/`

    The EPG login page will be displayed.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss18-Running%20Tomcat.png">

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss19-EPG%20Login%20Page.png">

    <br> <br>


### Logging In EPG

1. To login, first update user details by running following query in the `EPGLIVE` database:

    `update jfw_users set password='3d077da02dbcec2ccb8d8b0b82453a67' where username = 'full@HBL';`

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss23-Update%20User%20Details.png">

    <br> <br>


2. Then use the following user details to log in:

    Username: full@hbl
    Password: 123
    Tenant: HBL
    Locale: English

    After you are successfully logged in, the EPG landing page will appear.

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss20-EPG%20Logging%20In.png">

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss21-EPG%20Logged%20In%20Successfully.png">

    <img src="https://gitlab.com/ps.ujjwal.ranamagar/induction/-/raw/main/epg-screenshots/ss22-EPG%20Dashboard.png">

    <br> <br>